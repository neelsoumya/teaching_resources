# README #

Collection of teaching resources for complex systems, machine learning, computational biology and computational immunology.


Authors - Soumya Banerjee and Joyeeta Ghose




* Quick summary

	* Open source teaching materials for complex systems, data science, machine learning and computational biology.

	* If you use this code, please cite the paper and code

Banerjee, Soumya & Ghose, Joyeeta. (2018, January).
A Teaching Resource for Complex Systems, Machine Learning and Computational Biology.
Zenodo. http://doi.org/10.5281/zenodo.1098576


[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1098576.svg)](https://doi.org/10.5281/zenodo.1098576)


