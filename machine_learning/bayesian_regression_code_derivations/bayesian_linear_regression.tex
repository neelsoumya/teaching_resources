% Template for PLoS
% Version 1.0 January 2009
%
% To compile to pdf, run:
% latex plos.template
% bibtex plos.template
% latex plos.template
% latex plos.template
% dvipdf plos.template

\documentclass[10pt]{article}

% amsmath package, useful for mathematical formulas
\usepackage{amsmath}
% amssymb package, useful for mathematical symbols
\usepackage{amssymb}

% graphicx package, useful for including eps and pdf graphics
% include graphics with the command \includegraphics
\usepackage{graphicx}

% cite package, to clean up citations in the main text. Do not remove.
\usepackage{cite}

\usepackage{color} 


\usepackage{listings}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

% Use doublespacing - comment out for single spacing
\usepackage{setspace} 
\doublespacing

\usepackage{color}
\usepackage[usenames,dvipsnames]{xcolor}
\newcommand{\comment}[1]{{\color{red}\{#1\}}}


% Text layout
\topmargin 0.0cm
\oddsidemargin 0.5cm
\evensidemargin 0.5cm
\textwidth 16cm 
\textheight 21cm

% Bold the 'Figure #' in the caption and separate it with a period
% Captions will be left justified
\usepackage[labelfont=bf,labelsep=period,justification=raggedright]{caption}

% Use the PLoS provided bibtex style
\bibliographystyle{plos2009}

% Remove brackets from numbering in List of References
\makeatletter
\renewcommand{\@biblabel}[1]{\quad#1.}
\makeatother


% Leave date blank
\date{}

\pagestyle{myheadings}
%% ** EDIT HERE **


%% ** EDIT HERE **
%% PLEASE INCLUDE ALL MACROS BELOW

%% END MACROS SECTION

\begin{document}

% Title must be 150 characters or less
\begin{flushleft}
{\Large
\textbf{Teaching Materials for Bayesian Linear Regression}
}


% Insert Author names, affiliations and corresponding author email.
Soumya Banerjee $^{1\ast}$
\\
\bf{1} University of Oxford, Oxford, United Kingdom
\\


$\ast$ E-mail: soumya.banerjee@maths.ox.ac.uk
\end{flushleft}

% Please keep the abstract between 250 and 300 words
\section*{Abstract}

This tutorial outlines the basics and derivations of a Bayesian linear regression model.

\section*{Introduction}

Bayesian models can be used to derive estimates for noisy and sparse data if good priors can be imposed \cite{Banerjee2017g}.

Such models can be used more broadly in data science for social good \cite{Banerjee2018c}.



This document outlines the basics and derivations of a Bayesian linear regression model \cite{Box1992}. 

The derivations are adapted from an online resource \cite{bayesian_regression_youtube}.

Source code in the MATLAB programming language is also made freely available online \cite{banerjee_software_bayes_regression}.




\section*{Preliminaries}

The Bayesian inference approach can be described as follows. Assume that a model is represented by parameters $\Theta$. The Bayesian approach allows us to include prior knowledge about model parameters in a systematic fashion. If we have information about $\Theta$ (e.g., from experimental evidence) which needs to be incorporated in our analysis, this is represented as a prior probability distribution $P( \Theta)$. Bayes' Rule allows us to incorporate the prior knowledge about parameters, $P( \Theta)$, and experimental data, $D$, to derive a posterior distribution of parameters:


\begin{equation}
\label{bayes_eqn}
P(\Theta | D) = \frac{ P(D| \Theta) \cdot P(\Theta) } {P(D)}
\end{equation}



\section*{Derivations}

We define the problem of Bayesian linear regression as follows.

The likelihood term is

\begin{equation}
\label{eq_likelihood}
Y \sim N (X \beta, \Sigma )
\end{equation}

and the prior distribution on the parameters is

\begin{equation}
\label{eq_prior}
\beta \sim N ( 0, \Omega )
\end{equation}

where $Y$ is the data, $X$ is the design matrix and $\beta$ is a vector of the regressors.


The posterior distribution of the regressors will have the form 
\begin{equation}
\beta \sim N (\mu, \Lambda )
\end{equation}

where

\begin{equation}
\mu = \Lambda X^{T} \Sigma ^{-1} Y
\end{equation}

\begin{equation}
\Lambda = (X^{T} \Sigma ^{-1} X + \Omega^{-1})^{-1}
\end{equation}


The posterior distribution can be derived in the following way from a graphical model.

In a graphical model, for any node $u$, we can represent the remaining nodes by $U_{-u}$ and the full conditional distribution $P(u, U_{-u})$ is 
$\propto  P(u | parents[u]) \cdot  \prod_{w \in children[u]} P(w | parents[w])  $ \cite{gilks1995markov}. The full conditional distribution for $u$ contains a prior component (from the parents of $u$) and a likelihood component (from each child of $u$). We note that this is also a direct consequence of Bayes' Rule (Eq. \ref{bayes_eqn}).


\begin{equation}
P (\beta | Y)  \sim P(\beta)   P (Y | \beta  )
\end{equation}


Filling in the explicit forms of the prior (Eq. \ref{eq_prior}) and the likelihood (Eq. \ref{eq_likelihood}), we get 

\begin{equation}
P (\beta | Y)  \sim N (\beta | 0, \Omega )   N (Y | X \beta, \Sigma )
\end{equation}


Rearranging this we get


\begin{equation}
P (\beta | Y)  \sim   N (Y | X \beta, \Sigma ) N (\beta | 0, \Omega ) 
\end{equation}

Upon expansion with the explicit form of a Normal distribution, we get


\begin{equation}
P (\beta | Y)  \sim   e^{  (Y - X\beta)^{T} \Sigma^{-1} (Y - X\beta)  } e^{ \beta^{T}\Omega^{-1}\beta } 
\end{equation}


Focusing on the terms within the exponential, we get

\begin{equation}
  (Y - X\beta)^{T} \Sigma^{-1} (Y - X\beta)    +     \beta^{T}\Omega^{-1}\beta   
\end{equation}

Simplifying and noting that $(a + b)^{T} = a^{T} + b^{T}$, we get

\begin{equation}
=  (Y^{T} - (X\beta)^{T}) (\Sigma^{-1} Y - \Sigma^{-1} X\beta)    +     \beta^{T}\Omega^{-1}\beta   
\end{equation}

\begin{equation}
\label{compl_square1}
=  Y^{T}\Sigma^{-1} Y - Y^{T} \Sigma^{-1} X\beta - (X\beta)^{T}\Sigma^{-1}Y      +  (X\beta)^{T}\Sigma^{-1}X\beta +    \beta^{T}\Omega^{-1}\beta   
\end{equation}


We now complete the squares. 

The form of a normal distribution is 

\begin{equation}
N(x| \mu, \Lambda) \propto    \mathrm{exp}   \big\{ -\frac{1}{2}  \cdot (x - \mu)^{T} \Lambda^{-1} (x - \mu)  \big\}   
\end{equation}

Expanding the term within the exponential
\begin{equation}
  (x - \mu)^{T} \Lambda^{-1} (x - \mu)  
\end{equation}

Upon further rearrangement and noting that $(a + b)^{T} = a^{T} + b^{T}$ we get

\begin{equation}
\label{trick_compl_square}
=  x^{T}\Lambda^{-1}x   - 2x^{T}\Lambda^{-1}\mu + \mu^{T}\Lambda^{-1}\mu
\end{equation}

The last term is constant in x.

We now match Eq. \ref{compl_square1} to Eq. \ref{trick_compl_square} (complete the square).

Eq. \ref{compl_square1} is

\begin{equation}
=  Y^{T}\Sigma^{-1} Y - Y^{T} \Sigma^{-1} X\beta - (X\beta)^{T}\Sigma^{-1}Y      +  (X\beta)^{T}\Sigma^{-1}X\beta +    \beta^{T}\Omega^{-1}\beta   
\end{equation}

Rearranging we get
\begin{equation}
=  Y^{T}\Sigma^{-1} Y - 2(X\beta)^{T}\Sigma^{-1}Y  +   (X\beta)^{T}\Sigma^{-1}X\beta +   \beta^{T}\Omega^{-1}\beta   
\end{equation}

\begin{equation}
=  Y^{T}\Sigma^{-1} Y - 2(X\beta)^{T}\Sigma^{-1}Y  +   \beta^{T}X^{T}\Sigma^{-1}X\beta +   \beta^{T}\Omega^{-1}\beta   
\end{equation}

\begin{equation}
=  Y^{T}\Sigma^{-1} Y - 2(X\beta)^{T}\Sigma^{-1}Y  +   \beta^{T}(X^{T}\Sigma^{-1}X\beta +   \Omega^{-1}\beta)   
\end{equation}

\begin{equation}
=  Y^{T}\Sigma^{-1} Y - 2(X\beta)^{T}\Sigma^{-1}Y  +   \beta^{T}(X^{T}\Sigma^{-1}X +   \Omega^{-1})\beta   
\end{equation}


Matching $x^{T}\Lambda^{-1}x$ in Eq. \ref{trick_compl_square} to $\beta^{T}(X^{T}\Sigma^{-1}X +   \Omega^{-1})\beta$
we get
\begin{equation}
\Lambda^{-1} =  X^{T}\Sigma^{-1}X +   \Omega^{-1}  
\end{equation}

\begin{equation}
\Lambda =  (X^{T}\Sigma^{-1}X +   \Omega^{-1})^{-1}  
\end{equation}


Matching the term $2x^{T}\Lambda^{-1}\mu $ in Eq. \ref{trick_compl_square} to $2(X\beta)^{T}\Sigma^{-1}Y$

\begin{equation}
2x^{T}\Lambda^{-1}\mu = 2(X\beta)^{T}\Sigma^{-1}Y
\end{equation}

\begin{equation}
x^{T}\Lambda^{-1}\mu = \beta^{T}X^{T}\Sigma^{-1}Y
\end{equation}

$x$ is the same as $\beta$, hence we get

\begin{equation}
\Lambda^{-1}\mu = X^{T}\Sigma^{-1}Y
\end{equation}

and finally upon re-arranging 

\begin{equation}
\mu = \Lambda X^{T}\Sigma^{-1}Y
\end{equation}


Wrapping it all together we have Eq. \ref{compl_square1} as

\begin{equation}
\propto N(\mu,\Lambda) 
\end{equation}

The final posterior distribution is

\begin{equation}
\beta \sim N (\mu, \Lambda )
\end{equation}

where 
\begin{equation}
\mu = \Lambda X^{T}\Sigma^{-1}Y
\end{equation}

and

\begin{equation}
\Lambda =  (X^{T}\Sigma^{-1}X +   \Omega^{-1})^{-1}  
\end{equation}



\pagebreak


%\section*{References}
% The bibtex filename
\bibliography{cam_project}



\end{document}


